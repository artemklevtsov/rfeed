#' @title Функция для очистки и хэшированияч строки
#' @param x Строковый вектор.
#' @param key Строка с ключом для хэширования.
#' @return Строковый вектор.
hash_title <- function(x, key = NULL) {
  x <- tolower(x)
  x <- gsub("[^[:print:]]", "", x)
  x <- gsub("[[:punct:]]", "", x)
  x <- trimws(x)
  x <- openssl::sha1(x, key)
  return(x)
}

#' @title Функция для формирования сообщения по шаблону
#' @param data Таблица с данным RSS лент.
#' @param template Путь к файлу с шаблоном сообщения.
#' @return Строковый вектор.
make_message <- function(data, template = "msg.tmpl") {
  data <- data[, .(
    feed_title = htmltools::htmlEscape(feed_title),
    item_title = htmltools::htmlEscape(item_title),
    item_link = item_link
  )]

  tmpl <- readChar(template, file.size(template))
  res <- glue::glue_data(data, tmpl)
  return(res)
}
